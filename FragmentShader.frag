#version 330

// Interpolated values from the vertex shaders.
in vec2 uv;
in vec3 position_worldspace;
in vec3 normal_cameraspace;
in vec3 eyeDirection_cameraspace;
in vec3 lightDirection_cameraspace1;
in vec3 lightDirection_cameraspace2;

// Output data.
out vec4 color;

// Values that stay constant for the whole mesh.
uniform vec4 fcolor;
uniform vec3 lightPosition_worldspace1;
uniform vec3 lightPosition_worldspace2;
uniform sampler2D gSampler;

void main(void)
{
    // Light emission properties.
    vec4 lightColor1 = vec4(1.0, 1.0, 1.0, 1.0);
    float lightPower1 = 1250.0;

    vec4 lightColor2 = vec4(1.0, 1.0, 1.0, 1.0);
    float lightPower2 = 1250.0;

    // Material properties.
    vec4 materialDiffuseColor1 = fcolor;
    vec4 materialAmbientColor1 = vec4(0.1, 0.1, 0.1, 1.0) * materialDiffuseColor1;
    vec4 materialSpecularColor1 = vec4(0.3, 0.3, 0.3, 1.0);

    vec4 materialDiffuseColor2 = fcolor;
    vec4 materialAmbientColor2 = vec4(0.1, 0.1, 0.1, 1.0) * materialDiffuseColor2;
    vec4 materialSpecularColor2 = vec4(0.3, 0.3, 0.3, 1.0);

    // Distance to the light.
    float distance1 = length(lightPosition_worldspace1 - position_worldspace);
    float distance2 = length(lightPosition_worldspace2 - position_worldspace);

    // Normal of the computed fragment, in camera space.
    vec3 n = normalize(normal_cameraspace);
    // Direction of the light (from the fragment to the light).
    vec3 l1 = normalize(lightDirection_cameraspace1);
    vec3 l2 = normalize(lightDirection_cameraspace2);
    // Cosine of the angle between the normal and the light direction,
    // clamped above 0.
    //  - Light is at the vertical of the triangle -> 1.
    //  - Light is perpendicular to the triangle -> 0.
    //  - Light is behind the triangle -> 0.
    float cosTheta1 = clamp(dot(n, l1), 0.0, 1.0);
    float cosTheta2 = clamp(dot(n, l2), 0.0, 1.0);

    // Eye vector (towards the camera).
    vec3 e = normalize(eyeDirection_cameraspace);
    // Direction in which the triangle reflects the light.
    vec3 r1 = reflect(-l1, n);
    vec3 r2 = reflect(-l2, n);
    // Cosine of the angle between the Eye vector and the Reflect vector,
    // clamped to 0.
    //  - Looking into the reflection -> 1.
    //  - Looking elsewhere -> < 1.
    float cosAlpha1 = clamp(dot(e, r1), 0.0, 1.0);
    float cosAlpha2 = clamp(dot(e, r2), 0.0, 1.0);

    vec4 color1 =
            // Ambient: Simulates indirect lighting.
            materialAmbientColor1 +
            // Diffuse: "Color" of the object.
            materialDiffuseColor1 * lightColor1 * lightPower1 * cosTheta1 / (distance1 * distance1) +
            // Specular: Reflective highlight, like a mirror.
            materialSpecularColor1 * lightColor1 * lightPower1 * pow(cosAlpha1, 5.0) / (distance1 * distance1);

    vec4 color2 =
            // Ambient: Simulates indirect lighting.
            materialAmbientColor2 +
            // Diffuse: "Color" of the object.
            materialDiffuseColor2 * lightColor2 * lightPower2 * cosTheta2 / (distance2 * distance2) +
            // Specular: Reflective highlight, like a mirror.
            materialSpecularColor2 * lightColor2 * lightPower2 * pow(cosAlpha2, 5.0) / (distance2 * distance2);

    vec4 textureColor = texture(gSampler, uv);

    color = (color1 + color2) * textureColor;
}
