#version 330

// Input vertex data.
layout(location = 0) in vec3 vertexPosition_modelspace;
layout(location = 1) in vec2 vertexUV;
layout(location = 2) in vec3 vertexNormal_modelspace;

// Output data. Will be interpolated for each fragment.
out vec2 uv;
out vec3 position_worldspace;
out vec3 normal_cameraspace;
out vec3 eyeDirection_cameraspace;
out vec3 lightDirection_cameraspace1;
out vec3 lightDirection_cameraspace2;

// Values that stay constant for the whole mesh.
uniform mat4 mvp;
uniform mat4 v;
uniform mat4 m;
uniform vec3 lightPosition_worldspace1;
uniform vec3 lightPosition_worldspace2;

void main(void)
{
    // Output position of the vertex, in clip space: mvp * position.
    // Transform to a homogeneous 4D vector.
    gl_Position = mvp * vec4(vertexPosition_modelspace, 1.0);

    // Position of the vertex, in worldspace: m * position.
    position_worldspace = (m * vec4(vertexPosition_modelspace, 1.0)).xyz;

    // Vector that goes from the vertex to the camera, in camera space.
    // In camera space, the camera is at the origin (0.0, 0.0, 0.0).
    vec3 vertexPosition_cameraspace = (v * m * vec4(vertexPosition_modelspace, 1.0)).xyz;
    eyeDirection_cameraspace = vec3(0.0, 0.0, 0.0) - vertexPosition_cameraspace;

    // Vector that goes from the vertex to the light, in camera space. m is ommited, because it is the identity.
    vec3 lightPosition_cameraspace1 = (v * vec4(lightPosition_worldspace1, 1.0)).xyz;
    lightDirection_cameraspace1 = lightPosition_cameraspace1 + eyeDirection_cameraspace;

    // Vector that goes from the vertex to the light, in camera space. m is ommited, because it is the identity.
    vec3 lightPosition_cameraspace2 = (v * vec4(lightPosition_worldspace2, 1.0)).xyz;
    lightDirection_cameraspace2 = lightPosition_cameraspace2 + eyeDirection_cameraspace;

    // Normal of the vertex, in camera space.
    // Only correct if modelMatrix does not scale the model! Use it's inverse transpose if not.
    normal_cameraspace = (v * m * vec4(vertexNormal_modelspace, 0.0)).xyz;

    // UV of the vertex.
    uv = vertexUV;
}
