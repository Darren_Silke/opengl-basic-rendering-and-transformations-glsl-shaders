HEADERS       = glheaders.h \
                glwidget.h \ 
                texture.h

SOURCES       = glwidget.cpp \
                main.cpp \
                texture.cpp
QT += core gui opengl widgets
# Windows
LIBS += -l"C:\Program Files (x86)\Windows Kits\8.1\Lib\winv6.3\um\x64\OpenGL32"
LIBS += -l"C:\Program Files (x86)\Microsoft Visual Studio 12.0\VC\lib\Glew32"
LIBS += -l"C:\Program Files (x86)\Microsoft Visual Studio 12.0\VC\lib\FreeImage"
RESOURCES += resources.qrc
# Install
target.path = boom
INSTALLS += target
