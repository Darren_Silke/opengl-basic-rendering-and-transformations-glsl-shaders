Author:	        Darren Silke
Student Number: SLKDAR001
Date: 	        16 August 2015

Application Name: OpenGL - Basic Rendering And Transformations + GLSL Shaders

Description:

This application demonstrates the following:

1. Basic window setup.
2. Object loading.
3. Transformation.
4. Colour changes.
5. Phong-shading.
6. Shader files.
7. Moving lights.
8. Texturing.
9. Bump mapping.

This application makes use of Qt and it's OpenGL bindings. It therefore contains a Qmake file with the necessary library dependencies specified, such as GLEW (http://glew.sourceforge.net/) and FreeImage (http://freeimage.sourceforge.net/). 

Instructions:

1. Open up the project file called Games_3_GL3.2_Template.pro with Qt Creator. This is the Qmake file. Configure the project and make sure the debug and release folder that will be created are set for the current directory with all the other files. Ensure that the build is also configured to import the build from the current directory with all the other files.

2. Select Build->Run qmake, then select Build->Build All, and finally, select Build->Run to run the application.

3. Instructions on how to interact with the application are provided in the application output window in Qt creator.   They are also listed below.

How To Interact With The Application:

1. To scale the model, press 'S' on the keyboard. Use the mouse wheel to perform the action.
2. To translate the model, press 'T' followed by 'X', 'Y' or 'Z' on the keyboard. Use the mouse wheel to perform the action. The translation will be performed on the axis that was selected (X, Y or Z).
3. To rotate the model, press 'R' followed by 'X', 'Y' or 'Z' on the keyboard. Use the mouse wheel to perform the action. The rotation will be performed on the axis that was selected (X, Y or Z).
4. To change the colour of the model, press '1', '2', '3', '4' or '5' on the keyboard. The colours are respectively red, blue, Yellow, Magenta and Green.
5. To move the light sources, press 'F' to select the front light and 'B' to select the back light. Use the arrows keys on the keyboard to perform the action.
6. To reset the scene, right click in the window and select 'Reset Scene' from the pop-up menu.
7. To close the application, press the escape key on the keyboard or click the exit button at the top of the window.

List Of Files:

1.  FragmentShader.frag
2.  Games_3_GL3.2_Template.pro
3.  glheaders.h
4.  glwidget.cpp
5.  glwidget.h
6.  main.cpp
7.  Marvel  Ultimate Alliance-f456880.dds
8.  Marvel  Ultimate Alliance-f457900.dds
9.  Marvel  Ultimate Alliance-f458160.dds
10. README.txt
11. resources.qrc
12. texture.cpp
13. texture.h
14. thor.mtl
15. thor.obj
16. VertexShader.vert

List Of Folders:

1. .git
2. glm