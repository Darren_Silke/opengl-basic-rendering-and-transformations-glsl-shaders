#ifndef GLWIDGET_H
#define GLWIDGET_H

#include "glheaders.h" // Must be included before QT opengl headers.
#include "glm/glm/gtc/matrix_transform.hpp"
#include "texture.h"
#include <QGLWidget>
#include <QOpenGLBuffer>
#include <QOpenGLShaderProgram>
#include <sstream>

#define ESZ(elem) (int)elem.size()
#define RFOR(q, n) for(int q = n; q >= 0; q--)

class GLWidget : public QGLWidget
{
    Q_OBJECT
public:
    GLWidget(const QGLFormat& format, QWidget* parent = 0);
protected:
    virtual void initializeGL();
    virtual void resizeGL(int w, int h);
    virtual void paintGL();
    virtual void keyPressEvent(QKeyEvent* event);
    virtual void mousePressEvent(QMouseEvent* event);
    virtual void wheelEvent(QWheelEvent* event);
    virtual void resetScene();
private:
    bool prepareShaderProgram(const QString& vertexShaderPath, const QString& fragmentShaderPath);
    std::vector<std::string> split(std::string & s, std::string t);
    std::string getDirectoryPath(std::string & sFilePath);
    bool loadModel(std::string & sFileName, std::string & sMtlFileName, std::vector<glm::vec3> & outVertices, std::vector<glm::vec2> & outTexCoords, std::vector<glm::vec3> & outNormals);
    bool loadMaterial(std::string & sFullMtlFileName);

    QOpenGLShaderProgram m_shader;
    QOpenGLBuffer m_vertexBuffer;
    glm::mat4 modelMatrix;
    glm::mat4 viewMatrix;
    glm::mat4 projectionMatrix;
    glm::vec3 lightPosition1;
    glm::vec3 lightPosition2;
    GLuint matrixID;
    GLuint modelMatrixID;
    GLuint viewMatrixID;
    GLuint lightID1;
    GLuint lightID2;
    GLuint currentLightID;
    GLuint textureID;
    Texture tAmbientTexture;
    int numberOfVertices;
    int iAttrBitField;
    int iNumFaces;
    float scaleRatio;
    // Co-ordinates for center of bounding box.
    float centerX;
    float centerY;
    float centerZ;
    // Mode variable: S for scale mode, R for rotation mode and T for translation mode.
    char mode;    
    // Axis variable used to keep track of which axis to perform transformations on.
    // The options are X, Y or Z.
    char axis;
};
#endif // GLWIDGET_H
