#ifndef TEXTURE_H
#define TEXTURE_H

#include <GL/glew.h>
#include <Windows.h>
#include <FreeImage.h>
#include <string>

enum ETextureFiltering
{
    // Nearest criterion for magnification.
    TEXTURE_FILTER_MAG_NEAREST = 0,
    // Bilinear criterion for magnification.
    TEXTURE_FILTER_MAG_BILINEAR,
    // Nearest criterion for minification.
    TEXTURE_FILTER_MIN_NEAREST,
    // Bilinear criterion for minification.
    TEXTURE_FILTER_MIN_BILINEAR,
    // Nearest criterion for minification, but on closest mipmap.
    TEXTURE_FILTER_MIN_NEAREST_MIPMAP,
    // Bilinear criterion for minification, but on closest mipmap.
    TEXTURE_FILTER_MIN_BILINEAR_MIPMAP,
    // Bilinear criterion for minification on two closest mipmaps, then averaged.
    TEXTURE_FILTER_MIN_TRILINEAR
};

class Texture
{
public:
    Texture();
    void createFromData(BYTE* bData, int a_iWidth, int a_iHeight, int a_iBPP, GLenum format, bool bGenerateMipMaps = false);
    bool loadTexture2D(std::string a_sPath, bool bGenerateMipMaps = false);
    void bindTexture(int iTextureUnit = 0);
    void setFiltering(int a_tfMagnification, int a_tfMinification);
private:
    // Texture width, height and bytes per pixel.
    int iWidth, iHeight, iBPP;
    int tfMinification,tfMagnification;
    bool bMipMapsGenerated;
    // Texture name.
    UINT uiTexture;
    // Sampler name.
    UINT uiSampler;
    std::string sPath;
};
#endif // TEXTURE_H
