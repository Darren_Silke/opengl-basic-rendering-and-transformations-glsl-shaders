#include "glwidget.h"
#include <QCoreApplication>
#include <QKeyEvent>
#include <QMenu>

#define VERT_SHADER ":/VertexShader.vert"
#define FRAG_SHADER ":/FragmentShader.frag"

GLWidget::GLWidget(const QGLFormat& format, QWidget* parent)
    : QGLWidget(format, parent),
      m_vertexBuffer(QOpenGLBuffer::VertexBuffer)
{
    /*
     * Deliberately left empty.
     */
}
void GLWidget::initializeGL()
{
    // Resolve OpenGL functions.
    glewExperimental = true;
    GLenum GlewInitResult = glewInit();
    if(GlewInitResult != GLEW_OK)
    {
        const GLubyte* errorStr = glewGetErrorString(GlewInitResult);
        size_t size = strlen(reinterpret_cast<const char*>(errorStr));
        qDebug() << "Glew error "
                 << QString::fromUtf8(reinterpret_cast<const char*>(errorStr), (int)size);
    }

    // Get context OpenGL-version.
    qDebug() << "Widget OpenGl: " << format().majorVersion() << "." << format().minorVersion();
    qDebug() << "Context valid: " << context()->isValid();
    qDebug() << "Really used OpenGl: " << context()->format().majorVersion() << "." << context()->format().minorVersion();
    qDebug() << "OpenGl information: VENDOR:       " << (const char*)glGetString(GL_VENDOR);
    qDebug() << "                    RENDERDER:    " << (const char*)glGetString(GL_RENDERER);
    qDebug() << "                    VERSION:      " << (const char*)glGetString(GL_VERSION);
    qDebug() << "                    GLSL VERSION: " << (const char*)glGetString(GL_SHADING_LANGUAGE_VERSION);

    QGLFormat glFormat = QGLWidget::format();
    if (!glFormat.sampleBuffers())
    {
        qWarning() << "Could not enable sample buffers.";
    }

    // Set the clear color to black.
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    // Enable depth test.
    glEnable(GL_DEPTH_TEST);
    // Accept fragment if it is closer to the camera than the former one.
    glDepthFunc(GL_LESS);
    // Cull triangles which normal is not towards the camera.
    glEnable(GL_CULL_FACE);

    // There needs to be a VAO in core!
    GLuint VAO;
    glGenVertexArrays(1, &VAO);
    glBindVertexArray(VAO);

    // Model file name.
    std::string sFileName = "thor.obj";
    // Model material file name.
    std::string sMtlFileName = "thor.mtl";
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec2> textureCoordinates;
    std::vector<glm::vec3> normals;

    bool successful = loadModel(sFileName, sMtlFileName, vertices, textureCoordinates, normals);
    if(!successful)
    {
        qWarning() << "Failed to load" << QString::fromStdString(sFileName);
    }

    std::vector<float> points;

    for(int i = 0; i < vertices.size(); i++)
    {
        points.push_back(vertices[i].x);
        points.push_back(vertices[i].y);
        points.push_back(vertices[i].z);
        points.push_back(textureCoordinates[i].x);
        points.push_back(textureCoordinates[i].y);
        points.push_back(normals[i].x);
        points.push_back(normals[i].y);
        points.push_back(normals[i].z);
    }
    numberOfVertices = (int)points.size() / 8;

    // Load it into a VBO.
    GLuint VBO;
    glGenBuffers(1, &VBO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, (int)points.size() * sizeof(float), &points[0], GL_STATIC_DRAW);

    // First attribute buffer: Vertices.
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(
                0,                 // Attribute.
                3,                 // Size.
                GL_FLOAT,          // Type.
                GL_FALSE,          // Normalized?
                8 * sizeof(float), // Stride.
                (void*)0           // Array buffer offset.
                );

    // Second attribute buffer: Texture co-ordinates.
    const int tz = 3 * sizeof(float);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(
                1,                 // Attribute.
                2,                 // Size.
                GL_FLOAT,          // Type
                GL_FALSE,          // Normalized?
                8 * sizeof(float), // Stride.
                (void*)(tz)        // Array buffer offset.
                );

    // Third attribute buffer: Normals.
    const int nz = 5 * sizeof(float);
    glEnableVertexAttribArray(2);
    glVertexAttribPointer(
                2,                 // Attribute.
                3,                 // Size.
                GL_FLOAT,          // Type.
                GL_FALSE,          // Normalized?
                8 * sizeof(float), // Stride.
                (void*)(nz)        // Array buffer offset.
                );

    qDebug() << "Attempting vertex shader load from " << VERT_SHADER;
    qDebug() << "Attempting fragment shader load from " << FRAG_SHADER;

    // Prepare a complete shader program.
    if (!prepareShaderProgram(VERT_SHADER, FRAG_SHADER))
    {
        std::runtime_error("Failed to load shader.");
    }
    // Bind the shader program so that we can associate variables from our application to the shaders.
    if (!m_shader.bind())
    {
        qWarning() << "Could not bind shader program to context.";
        return;
    }
    // Enable the "vertexPosition_modelspace" attribute to bind it to the currently bound vertex buffer.
    m_shader.setAttributeBuffer("vertexPosition_modelspace", GL_FLOAT, 0, 8);
    m_shader.enableAttributeArray("vertexPosition_modelspace");

    // Enable the "vertexUV" attribute to bind it to the currently bound vertex buffer.
    m_shader.setAttributeBuffer("vertexUV", GL_FLOAT, 0, 8);
    m_shader.enableAttributeArray("vertexUV");

    // Enable the "vertexNormal_modelspace" attribute to bind it to the currently bound vertex buffer.
    m_shader.setAttributeBuffer("vertexNormal_modelspace", GL_FLOAT, 0, 8);
    m_shader.enableAttributeArray("vertexNormal_modelspace");

    // Setup the first light.
    lightID1 = glGetUniformLocation(m_shader.programId(), "lightPosition_worldspace1");
    lightPosition1 = glm::vec3(0, 0, 40);

    // Setup the second light.
    lightID2 = glGetUniformLocation(m_shader.programId(), "lightPosition_worldspace2");
    lightPosition2 = glm::vec3(0, 0, -40);

    // Set the initial color of the model.
    glUniform4f(glGetUniformLocation(m_shader.programId(),"fcolor"),0.0f,1.0f,1.0f,1.0f);

    // Creation of a bounding box to ensure that the object rendered will be at the center of the window.
    float minX = 0.0f;
    float minY = 0.0f;
    float minZ = 0.0f;
    float maxX = 0.0f;
    float maxY = 0.0f;
    float maxZ = 0.0f;

    for(int i = 0; i < vertices.size(); i++)
    {
        if(vertices[i].x < minX)
        {
            minX = vertices[i].x;
        }
        if(vertices[i].x > maxX)
        {
            maxX = vertices[i].x;
        }
        if(vertices[i].y < minY)
        {
            minY = vertices[i].y;
        }
        if(vertices[i].y > maxY)
        {
            maxY = vertices[i].y;
        }
        if(vertices[i].z < minZ)
        {
            minZ = vertices[i].z;
        }
        if(vertices[i].z > maxZ)
        {
            maxZ = vertices[i].z;
        }
    }

    centerX = -(minX + maxX)/2.0f;
    centerY = -(minY + maxY)/2.0f;
    centerZ = -(minZ + maxZ)/2.0f;

    // Projection matrix: 45° field of view, 4:3 ratio, display range: 0.1 unit <-> 500 units.
    projectionMatrix = glm::perspective(45.0f, 4.0f / 3.0f, 0.1f, 500.0f);
    // Camera matrix.
    viewMatrix = glm::lookAt(
                 glm::vec3(0, 0, 75), // Camera is at (0, 0, 75) in World Space.
                 glm::vec3(0, 0, 0),  // Camera looks at the origin.
                 glm::vec3(0, 1, 0)   // Head is up.
                 );
    // Model matrix.
    modelMatrix = glm::mat4(1.0f);
    modelMatrix = glm::rotate(modelMatrix, -89.55f, glm::vec3(0.0f, 1.0f, 0.0f));
    modelMatrix = glm::translate(modelMatrix, glm::vec3(centerX, centerY, centerZ));
    // ModelViewProjection -> Multiplication of 3 matrices.
    glm::mat4 mvp = projectionMatrix * viewMatrix * modelMatrix;

    // Get a handle for "mvp" uniform.
    matrixID = glGetUniformLocation(m_shader.programId(), "mvp");
    viewMatrixID = glGetUniformLocation(m_shader.programId(), "v");
    modelMatrixID = glGetUniformLocation(m_shader.programId(), "m");

    // Send the transformation to the currently bound shader in the "mvp" uniform.
    glUniformMatrix4fv(matrixID, 1, GL_FALSE, &mvp[0][0]);
    glUniformMatrix4fv(modelMatrixID, 1, GL_FALSE, &modelMatrix[0][0]);
    glUniformMatrix4fv(viewMatrixID, 1, GL_FALSE, &viewMatrix[0][0]);

    // Set the initial scale ratio.
    scaleRatio = 1.0f;

    // Bind the current texture.
    tAmbientTexture.bindTexture();
    textureID = glGetUniformLocation(m_shader.programId(), "gSampler");
    glUniform1i(textureID, 0);

    // Output instructions on how to interact with the GUI.
    qDebug() << "\nINSTRUCTIONS:\n";
    qDebug() << "1. To scale the model, press 'S' on the keyboard. Use the mouse wheel to perform the action.";
    qDebug() << "2. To translate the model, press 'T' followed by 'X', 'Y' or 'Z' on the keyboard. Use the mouse wheel to perform the action.";
    qDebug() << "   The translation will be performed on the axis that was selected (X, Y or Z).";
    qDebug() << "3. To rotate the model, press 'R' followed by 'X', 'Y' or 'Z' on the keyboard. Use the mouse wheel to perform the action.";
    qDebug() << "   The rotation will be performed on the axis that was selected (X, Y or Z).";
    qDebug() << "4. To change the colour of the model, press '1', '2', '3', '4' or '5' on the keyboard.";
    qDebug() << "   The colours are respectively red, blue, Yellow, Magenta and Green.";
    qDebug() << "5. To move the light sources, press 'F' to select the front light and 'B' to select the back light.";
    qDebug() << "   Use the arrow keys on the keyboard to perform the action.";
    qDebug() << "6. To reset the scene, right click in the window and select 'Reset Scene' from the pop-up menu.";
    qDebug() << "7. To close the application, press the escape key on the keyboard or click the exit button at the top of the window.";
}
void GLWidget::resizeGL(int w, int h)
{
    // Set the viewport to window dimensions.
    glViewport(0, 0, w, qMax(h, 1));
}
void GLWidget::paintGL()
{
    // Clear the buffer with the current clearing color.
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glUniform3f(lightID1, lightPosition1.x, lightPosition1.y, lightPosition1.z);
    glUniform3f(lightID2, lightPosition2.x, lightPosition2.y, lightPosition2.z);

    // Draw stuff.
    glDrawArrays(GL_TRIANGLES, 0, numberOfVertices);
}
void GLWidget::keyPressEvent(QKeyEvent* event)
{
    switch(event->key())
    {
        // Close the application.
        case Qt::Key_Escape:
            QCoreApplication::instance()->quit();
            break;

        // Switch to scale mode.
        case Qt::Key_S:
            mode = 'S';
            break;

        // Switch to rotation mode.
        case Qt::Key_R:
            mode = 'R';
            break;

        // Switch to translation mode.
        case Qt::Key_T:
            mode = 'T';
            break;

        // Switch to x-axis.
        case Qt::Key_X:
            axis = 'X';
            break;

        // Switch to y-axis.
        case Qt::Key_Y:
            axis = 'Y';
            break;

        // Switch to z-axis.
        case Qt::Key_Z:
            axis = 'Z';
            break;

        // Change the colour of the model.
        case Qt::Key_1:
            // Red.
            glUniform4f(glGetUniformLocation(m_shader.programId(),"fcolor"),1.0f,0.0f,0.0f,1.0f);
            update();
            break;

        // Change the colour of the model.
        case Qt::Key_2:
            // Blue.
            glUniform4f(glGetUniformLocation(m_shader.programId(),"fcolor"),0.0f,0.0f,1.0f,1.0f);
            update();
            break;

        // Change the colour of the model.
        case Qt::Key_3:
            // Yellow.
            glUniform4f(glGetUniformLocation(m_shader.programId(),"fcolor"),1.0f,1.0f,0.0f,1.0f);
            update();
            break;

        // Change the colour of the model.
        case Qt::Key_4:
            // Magenta.
            glUniform4f(glGetUniformLocation(m_shader.programId(),"fcolor"),1.0f,0.0f,1.0f,1.0f);
            update();
            break;

        // Change the colour of the model.
        case Qt::Key_5:
            // Green.
            glUniform4f(glGetUniformLocation(m_shader.programId(),"fcolor"),0.0f,1.0f,0.0f,1.0f);
            update();
            break;

        // Activate the front light controller.
        case Qt::Key_F:
            currentLightID = lightID1;
            break;

        // Activate the back light controller.
        case Qt::Key_B:
            currentLightID = lightID2;
            break;

        case Qt::Key_Up:
            if(currentLightID == lightID1)
                lightPosition1.y += 1;
            else if(currentLightID == lightID2)
                lightPosition2.y += 1;
            update();
            break;

        case Qt::Key_Down:
            if(currentLightID == lightID1)
                lightPosition1.y -= 1;
            else if(currentLightID == lightID2)
                lightPosition2.y -= 1;
            update();
            break;

        case Qt::Key_Left:
            if(currentLightID == lightID1)
                lightPosition1.x -= 1;
            else if(currentLightID == lightID2)
                lightPosition2.x += 1;
            update();
            break;

        case Qt::Key_Right:
            if(currentLightID == lightID1)
                lightPosition1.x += 1;
            else if(currentLightID == lightID2)
                lightPosition2.x -= 1;
            update();
            break;

        default:
            QGLWidget::keyPressEvent(event);
            break;
    }
}
void GLWidget::mousePressEvent(QMouseEvent* event)
{
    if(event->button() == Qt::RightButton)
    {
        QAction resetSceneAction("Reset Scene", this);
        connect(&resetSceneAction, &QAction::triggered, this, &GLWidget::resetScene);

        QMenu menu;
        menu.addAction(&resetSceneAction);
        menu.addSeparator();
        menu.exec(mapToGlobal(event->pos()));
    }
    QGLWidget::mousePressEvent(event);
}
void GLWidget::wheelEvent(QWheelEvent *event)
{
    if(event->orientation() == Qt::Vertical)
    {
        // Scale mode.
        if(mode == 'S')
        {
            scaleRatio += (float)(event->delta()/1000.0f);
            if(scaleRatio <= 0)
            {
                scaleRatio -= (float)(event->delta()/1000.0f);
            }
            modelMatrix = glm::mat4(1.0f);
            modelMatrix = glm::rotate(modelMatrix, -89.55f, glm::vec3(0.0f, 1.0f, 0.0f));
            modelMatrix = glm::scale(modelMatrix, glm::vec3(scaleRatio, scaleRatio, scaleRatio));
            modelMatrix = glm::translate(modelMatrix, glm::vec3(centerX, centerY, centerZ));
        }
        // Rotation mode.
        else if(mode == 'R')
        {
            if(axis == 'X')
            {
                if(event->delta() > 0)
                {
                    modelMatrix = glm::rotate(modelMatrix, -0.25f, glm::vec3(0.0f, 0.0f, 1.0f));
                }
                else
                {
                    modelMatrix = glm::rotate(modelMatrix, 0.25f, glm::vec3(0.0f, 0.0f, 1.0f));
                }
            }
            else if(axis == 'Y')
            {
                if(event->delta() > 0)
                {                    
                    modelMatrix = glm::rotate(modelMatrix, -0.25f, glm::vec3(0.0f, 1.0f, 0.0f));
                }
                else
                {
                    modelMatrix = glm::rotate(modelMatrix, 0.25f, glm::vec3(0.0f, 1.0f, 0.0f));
                }
            }
            else if(axis == 'Z')
            {
                if(event->delta() > 0)
                {
                    modelMatrix = glm::rotate(modelMatrix, -0.25f, glm::vec3(1.0f, 0.0f, 0.0f));
                }
                else
                {            
                    modelMatrix = glm::rotate(modelMatrix, 0.25f, glm::vec3(1.0f, 0.0f, 0.0f));
                }
            }
        }
        // Translation mode.
        else if(mode == 'T')
        {
            if(axis == 'X')
            {
                if(event->delta() > 0)
                {
                    modelMatrix = glm::translate(modelMatrix, glm::vec3(0.0f, 0.0f, -1.0f));
                }
                else
                {
                    modelMatrix = glm::translate(modelMatrix, glm::vec3(0.0f, 0.0f, 1.0f));
                }
            }
            else if(axis == 'Y')
            {
                if(event->delta() > 0)
                {
                    modelMatrix = glm::translate(modelMatrix, glm::vec3(0.0f, 1.0f, 0.0f));
                }
                else
                {
                    modelMatrix = glm::translate(modelMatrix, glm::vec3(0.0f, -1.0f, 0.0f));
                }
            }
            else if(axis == 'Z')
            {
                if(event->delta() > 0)
                {
                    modelMatrix = glm::translate(modelMatrix, glm::vec3(1.0f, 0.0f, 0.0f));
                }
                else
                {
                    modelMatrix = glm::translate(modelMatrix, glm::vec3(-1.0f, 0.0f, 0.0f));
                }
            }
        }

        // ModelViewProjection -> Multiplication of 3 matrices.
        glm::mat4 mvp = projectionMatrix * viewMatrix * modelMatrix;
        // Send the transformation to the currently bound shader in the "mvp" uniform.
        glUniformMatrix4fv(matrixID, 1, GL_FALSE, &mvp[0][0]);
        update();
    }
    QGLWidget::wheelEvent(event);
}
void GLWidget::resetScene()
{
    // Reset the model matrix.
    modelMatrix = glm::mat4(1.0f);
    modelMatrix = glm::rotate(modelMatrix, -89.55f, glm::vec3(0.0f, 1.0f, 0.0f));
    modelMatrix = glm::translate(modelMatrix, glm::vec3(centerX, centerY, centerZ));
    // Reset the scale ratio.
    scaleRatio = 1.0f;
    // Reset the mode.
    mode = NULL;
    // Reset the axis.
    axis = NULL;
    // Reset the current light ID.
    currentLightID = 0;
    // Reset the light positions.
    lightPosition1 = glm::vec3(0, 0, 40);
    lightPosition2 = glm::vec3(0, 0, -40);

    // ModelViewProjection -> Multiplication of 3 matrices.
    glm::mat4 mvp = projectionMatrix * viewMatrix * modelMatrix;

    // Send the transformation to the currently bound shader in the "mvp" uniform.
    glUniformMatrix4fv(matrixID, 1, GL_FALSE, &mvp[0][0]);
    glUniform4f(glGetUniformLocation(m_shader.programId(),"fcolor"),0.0f,1.0f,1.0f,1.0f);
    update();
}
bool GLWidget::prepareShaderProgram(const QString& vertexShaderPath, const QString& fragmentShaderPath)
{
    // First we load and compile the vertex shader.
    bool result = m_shader.addShaderFromSourceFile(QOpenGLShader::Vertex, vertexShaderPath);
    if (!result)
    {
        qWarning() << m_shader.log();
    }

    // Now the fragment shader.
    result = m_shader.addShaderFromSourceFile(QOpenGLShader::Fragment, fragmentShaderPath);
    if (!result)
    {
        qWarning() << m_shader.log();
    }

    // Finally we link them to resolve any references.
    result = m_shader.link();
    if (!result)
    {
        qWarning() << "Could not link shader program:" << m_shader.log();
    }

    return result;
}
std::vector<std::string> GLWidget::split(std::string & s, std::string t)
{
    std::vector<std::string> res;
    while(1)
    {
        int pos = (int)s.find(t);
        if(pos == -1)
        {
            res.push_back(s);
            break;
        }
        res.push_back(s.substr(0, pos));
        s = s.substr(pos + 1, ESZ(s) - pos - 1);
    }
    return res;
}
std::string GLWidget::getDirectoryPath(std::string & sFilePath)
{
    // Get directory path.
    std::string sDirectory = "";
    RFOR(i, ESZ(sFilePath) - 1)if(sFilePath[i] == '\\' || sFilePath[i] == '/')
    {
        sDirectory = sFilePath.substr(0, i + 1);
        break;
    }
    return sDirectory;
}
bool GLWidget::loadModel(std::string & sFileName, std::string & sMtlFileName, std::vector<glm::vec3> & outVertices, std::vector<glm::vec2> & outTexCoords, std::vector<glm::vec3> & outNormals)
{
    FILE* fp = fopen(sFileName.c_str(), "rt");

    if(fp == nullptr)
        return false;

    char line[255];

    std::vector<glm::vec3> vVertices;
    std::vector<glm::vec2> vTexCoords;
    std::vector<glm::vec3> vNormals;

    iNumFaces = 0;

    while(fgets(line, 255, fp))
    {
        // Error flag, that can be set when something is inconsistent throughout
        // data parsing.
        bool bError = false;

        // If it's an empty line, then skip.
        if(strlen(line) <= 1)
            continue;

        // Now process line.
        std::stringstream ss(line);
        std::string sType;
        ss >> sType;
        // If it's a comment, skip it.
        if(sType == "#")
            continue;
        // Vertex.
        else if(sType == "v")
        {
            glm::vec3 vNewVertex;
            int dim = 0;
            while(dim < 3 && ss >> vNewVertex[dim])
                dim++;
            vVertices.push_back(vNewVertex);
            iAttrBitField |= 1;
        }
        // Texture co-ordinate.
        else if(sType == "vt")
        {
            glm::vec2 vNewCoord;
            int dim = 0;
            while(dim < 2 && ss >> vNewCoord[dim])
                dim++;
            vTexCoords.push_back(vNewCoord);
            iAttrBitField |= 2;
        }
        // Normal.
        else if(sType == "vn")
        {
            glm::vec3 vNewNormal;
            int dim = 0;
            while(dim < 3 && ss >> vNewNormal[dim])
                dim++;
            vNewNormal = glm::normalize(vNewNormal);
            vNormals.push_back(vNewNormal);
            iAttrBitField |= 4;
        }
        // Face definition.
        else if(sType == "f")
        {
            std::string sFaceData;
            // This will run for as many vertex definitions as the face has
            // (for triangle, it's 3).
            while(ss >> sFaceData)
            {
                std::vector<std::string> data = split(sFaceData, "/");
                int iVertIndex = -1, iTexCoordIndex = -1, iNormalIndex = -1;

                // If there were some vertices defined earlier.
                if(iAttrBitField&1 && !bError)
                {
                    if(ESZ(data[0]) > 0)
                        sscanf(data[0].c_str(), "%d", &iVertIndex);
                    else
                        bError = true;
                }
                // If there were some texture co-ordinates defined earlier.
                if(iAttrBitField&2 && !bError)
                {
                    if(ESZ(data) >= 1)
                    {
                        // Just a check whether face format isn't f v//vn.
                        // In that case, data[1] is empty string.
                        if(ESZ(data[1]) > 0)
                            sscanf(data[1].c_str(), "%d", &iTexCoordIndex);
                        else
                            bError = true;
                    }
                    else
                        bError = true;
                }
                // If there were some normals defined earlier.
                if(iAttrBitField&4 && !bError)
                {
                    if(ESZ(data) >= 2)
                    {
                        if(ESZ(data[2]) > 0)
                            sscanf(data[2].c_str(), "%d", &iNormalIndex);
                        else
                            bError = true;
                    }
                    else
                        bError = true;
                }
                if(bError)
                {
                    fclose(fp);
                    return false;
                }

                // Check whether vertex index is within boundaries (indexed from 1).
                if(iVertIndex > 0 && iVertIndex <= ESZ(vVertices))
                    outVertices.push_back(vVertices[iVertIndex - 1]);
                if(iTexCoordIndex > 0 && iTexCoordIndex <= ESZ(vTexCoords))
                    outTexCoords.push_back(vTexCoords[iTexCoordIndex - 1]);
                if(iNormalIndex > 0 && iNormalIndex <= ESZ(vNormals))
                    outNormals.push_back(vNormals[iNormalIndex - 1]);
            }
            iNumFaces++;
        }
        // Shading model, for now just skip it.
        else if(sType == "s")
        {
            // Do nothing for now.
        }
        // Material specified, skip it again.
        else if(sType == "usemtl")
        {
            // Do nothing for now.
        }
    }

    fclose(fp);

    if(iAttrBitField == 0)
        return false;

    // Material should be in the same directory as model.
    bool successful = loadMaterial(getDirectoryPath(sFileName) + sMtlFileName);
    if(!successful)
    {
        qWarning() << "Failed to load" << QString::fromStdString(sMtlFileName);
    }

    return true;
}
bool GLWidget::loadMaterial(std::string & sFullMtlFileName)
{
    // For now, just look for ambient texture, i.e. line that begins with 'map_Ka'.
    FILE* fp = fopen(sFullMtlFileName.c_str(), "rt");

    if(fp == nullptr)
        return false;

    char line[255];

    while(fgets(line, 255, fp))
    {
        std::stringstream ss(line);
        std::string sType;
        ss >> sType;
        if(sType == "map_Ka")
        {
            std::string sLine = line;
            // Take the rest of line as texture name, remove newline character from end.
            int from = (int)sLine.find("map_Ka") + 6 + 1;
            std::string sTextureName = sLine.substr(from, ESZ(sLine) - from - 1);
            // Texture should be in the same directory as material.
            tAmbientTexture.loadTexture2D(getDirectoryPath(sFullMtlFileName) + sTextureName, true);
            tAmbientTexture.setFiltering(TEXTURE_FILTER_MAG_BILINEAR, TEXTURE_FILTER_MIN_NEAREST_MIPMAP);
            break;
        }
    }
    fclose(fp);

    return true;
}
